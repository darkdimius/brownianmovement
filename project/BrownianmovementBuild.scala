import sbt._
import sbt.Keys._
import com.typesafe.sbt.SbtProguard._


object BrownianmovementBuild extends Build {
  import ProguardKeys.{ mergeStrategies, merge, options }
  import ProguardOptions.keepMain
  import ProguardMerge.append

  lazy val proguard = proguardSettings ++ Seq(
   options := Seq(keepMain("me.dd.Brownianmovement"), """
-keep public class akka.actor.LocalActorRefProvider {
  public <init>(...);
}
-keep public class akka.remote.RemoteActorRefProvider {
  public <init>(...);
}
-keep class akka.actor.SerializedActorRef {
  *;
}
-keep class akka.remote.netty.NettyRemoteTransport {
  *;
}
-keep class akka.serialization.JavaSerializer {
  *;
}
-keep class akka.serialization.ProtobufSerializer {
  *;
}
-keep class com.google.protobuf.GeneratedMessage {
  *;
}
-keep class akka.event.Logging*
-keep class akka.event.Logging$LogExt{
  *;
}
""")  )

  lazy val brownianmovement = Project(
    id = "brownianmovement",
    base = file("."),
    settings = Project.defaultSettings ++ Seq(
      name := "BrownianMovement",
      organization := "me.dd",
      version := "0.2-SNAPSHOT",
      scalaVersion := "2.9.3",
      resolvers += "Typesafe Releases" at "http://repo.typesafe.com/typesafe/releases",
      libraryDependencies += "com.typesafe.akka" % "akka-actor" % "2.0.5",
      libraryDependencies += "org.rogach" %% "scallop" % "0.9.4"
    ) ++ proguard
  )
}
