package me.dd

import akka.actor._
import akka.pattern.{AskTimeoutException, ask}
import akka.util.duration._
import akka.util.{Duration, Timeout}
import annotation.tailrec
import akka.dispatch.{Future, Await}
import java.util.concurrent.atomic.AtomicIntegerArray
import org.rogach.scallop.{Subcommand, ScallopConf}

case object Tick

case object Get

case class LocationReport(location: Int, iteration: Long)
class ExecutionMode

case class LimitedTime(limit: Duration, delay: Duration) extends ExecutionMode

case class LimitedIterations(limit: Long) extends ExecutionMode

class BrownianExperiment(boxes: Int, particles: Int, rightDriftProbability: Double, mode: ExecutionMode) {



  class ParticleContainer(reporter: ActorRef) extends Actor {
    var location = boxes / 2

    def until[T](predicate: (Long, Long) => Boolean)(action: () => T) {
      @tailrec
      def until0(currentIteration: Long = 0,
                 initialTime: Long = System.currentTimeMillis()) {
        if (!predicate(currentIteration, initialTime)) {
          action()
          until0(currentIteration + 1, initialTime)
        }

      }
      until0()
    }

    def iterationEnded(currentIteration: Long, initialTime: Long) = {
      mode match {
        case LimitedIterations(limit) => 
          if ((currentIteration % (limit/10) == 0) || currentIteration == limit) reporter ! LocationReport(location, currentIteration)
          currentIteration >= limit
        case LimitedTime(limit, delay) => 
          reporter ! LocationReport(location, currentIteration)
          limit.toMillis + initialTime <= System.currentTimeMillis()
      }
    }

    def step() {
      location += (if (math.random < rightDriftProbability) 1 else -1)
      if (location < 0) location = 0
      else if (location == boxes) location = boxes - 1
    }

    def receive = {
      case Tick => until(iterationEnded _) {
        step
      }
      case Get => sender ! location
    }
  }
3
  class StatusPrinter extends Actor {

    val states = collection.mutable.Map[Long, Array[Int]]()

    def printAllLocations(iteration: Long) = {
      println("\nState at time:" + iteration)
          states(iteration).foreach { x =>
          print(x + " ")
        }
      print("\n")
    }
    
    def receive = {
      case LocationReport(pos, time) => 
        states.get(time) match {
          case Some(x) => x(pos) += 1
          case None => 
            states(time) = Array.fill(boxes)(0)
            states(time)(pos) = 1
        }
        if(states(time).sum == particles) {
          printAllLocations(time)
          states(time) = null
        }
    }
  }
}



object Brownianmovement extends App {
  var arg = args
  val start =  System.currentTimeMillis()
  if (arg.size == 0) {
    arg = Array("--help")
  }

  object Conf extends ScallopConf(arg) {
    version("BrownianExperiment 0.0.99  2013 Dmitry Petrashko")
    banner( """runMain me.dd.Brownianmovement -b 1 -p 1 -r 0.5  [time|iter] -l 1000
              |Options:
              | """.stripMargin)
    footer("\nFor all other tricks, consult the sourcecode!")
    // ... options ...
    val boxes = opt[Int]("boxes", descr = "how many boxes do you need?", required = true)
    val particles = opt[Int]("particles", descr = "how many particles do you need?", required = true)
    val time = new Subcommand("time") {
      val limit = opt[Int]("limit", descr = "how much milliseconds should every particle move?", required = true)
//      val delay = opt[Int]("delay", descr = "how much milliseconds should every particle wait after iteration?", required = true)
    }
    val iter = new Subcommand("iter") {
      val limit = opt[Int]("limit", descr = "how much iterations should every particle make", required = true)
    }
    val help = new Subcommand("help")

    val rightShiftProbability = opt[Double]("rightShiftProbability",
      descr = "probability of particle moving rigth in iteration", required = true)

  }

  println(Conf.summary)
//  Conf.verify()
  val system = ActorSystem("Brownianmovement")
  val size = Conf.boxes()


  val mode = Conf.subcommand match {
    case Some(Conf.iter) => LimitedIterations(Conf.iter.limit())
    case Some(Conf.time) => LimitedTime(Conf.time.limit() milliseconds, /*Conf.time.delay()*/ 0 milliseconds)
    case None => println("Mode should be specefied")
    Conf.printHelp()
    sys.exit(1)
  }
  val particles = Conf.particles()


  val experiment = new BrownianExperiment(size, particles, Conf.rightShiftProbability(), mode)
  val reporterProps = Props(new UntypedActorFactory {
    @scala.throws(classOf[Exception])
    def create() = new experiment.StatusPrinter
  })
  val reporter = system.actorOf(reporterProps)
  val counterProps = Props(new UntypedActorFactory {
    @scala.throws(classOf[Exception])
    def create() = new experiment.ParticleContainer(reporter)
  })

  val counter = Array.fill(particles) {
    system.actorOf(counterProps)
  }

  counter.foreach {
    _ ! Tick
  }


  //println("\nFinal State:")
  implicit val timeout = Timeout(10 minutes)
  val boxes = new AtomicIntegerArray(size)

  val results = counter.map {
    x: ActorRef =>
      (x ? Get) onSuccess {
        case count: Int =>
          //println("Count is " + count)
          boxes.addAndGet(count, 1)
      } onFailure {
        case e: AskTimeoutException =>
          println("timedOut to get result " + x)
      }
  }
  results.foreach {
    x: Future[Any] => Await.ready(x, 10 minutes)
  }
  (0 until boxes.length())
    .foreach {
    x =>
      print(boxes.get(x) + " ")
  }
  println("Execution time: " +  (System.currentTimeMillis() - start) + " ms")
  system.shutdown()
}
